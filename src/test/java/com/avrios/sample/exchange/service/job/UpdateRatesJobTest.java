package com.avrios.sample.exchange.service.job;

import com.avrios.sample.exchange.service.retrieval.CubeType;
import com.avrios.sample.exchange.service.retrieval.EcbClient;
import com.avrios.sample.exchange.service.storage.ExchangeRateService;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.avrios.sample.exchange.service.storage.CurrencyKey.euroCurrencyKey;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UpdateRatesJobTest {

  @Mock
  private EcbClient ecbClient;
  @Mock
  private ExchangeRateService rateService;

  private UpdateRatesJob job;

  @Before
  public void setUp() throws Exception {
    job = new UpdateRatesJob(ecbClient, rateService);
  }

  @Test
  public void shouldExecuteUpdateDailyRatesJobIfRatesForTodayNotStored() throws Exception {
    when(ecbClient.retrieveTodaysRates()).thenReturn(create());

    job.updateDailyRates();

    verify(ecbClient, times(1)).retrieveTodaysRates();
    verify(rateService, times(1)).add(euroCurrencyKey("PLN"), LocalDate.now(), BigDecimal.TEN);
  }

  @Test
  public void shouldNotExecuteUpdateDailyRatesJobIfRatesForTodayAlreadyStored() throws Exception {
    when(rateService.hasRatesFor(LocalDate.now())).thenReturn(true);

    job.updateDailyRates();

    verify(ecbClient, never()).retrieveTodaysRates();
    verify(rateService, never()).add(any(), any(), any());
  }

  @Test
  public void shouldExecuteUpdateLast90DaysRatesJobIfRatesForLastMondayNotStored() throws Exception {
    when(ecbClient.retrieveLast90DaysRates()).thenReturn(Lists.newArrayList(create()));

    job.updateLast90DaysRates();

    verify(ecbClient, times(1)).retrieveLast90DaysRates();
    verify(rateService, times(1)).add(euroCurrencyKey("PLN"), LocalDate.parse("2017-04-24"), BigDecimal.TEN);
  }

  @Test
  public void shouldNotExecuteUpdateLast90DaysRatesJobIfRatesForLastMondayAlreadyStored() throws Exception {
    when(rateService.hasRatesFor(any())).thenReturn(true);

    job.updateLast90DaysRates();

    verify(ecbClient, never()).retrieveLast90DaysRates();
    verify(rateService, never()).add(any(), any(), any());
  }


  private CubeType create() {
    CubeType parent = new CubeType();
    CubeType cubeType = new CubeType();
    cubeType.setCurrency("PLN");
    cubeType.setRate("10");
    parent.setTime("2017-04-24");
    parent.setContent(Lists.newArrayList(cubeType));
    return parent;
  }
}