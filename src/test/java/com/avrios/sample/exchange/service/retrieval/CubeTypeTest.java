package com.avrios.sample.exchange.service.retrieval;

import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CubeTypeTest {

  @Test
  public void shouldMapDailyXmlToCubeTypeList() throws Exception {
    List<CubeType> cubeType = readAndMapFile("/eurofxref-daily.xml").getRates();

    assertThat(cubeType).isNotNull();
    assertThat(cubeType.get(0).getTime()).isEqualTo("2017-04-28");
  }

  @Test
  public void shouldMap90dXmlToCubeTypeList() throws Exception {
    List<CubeType> cubeType = readAndMapFile("/eurofxref-hist-90d.xml").getRates();

    assertThat(cubeType).isNotNull();
    assertThat(cubeType.get(0).getTime()).isEqualTo("2017-04-28");
  }

  private CubeTypeWrapper readAndMapFile(String filePath) throws Exception {
    JAXBContext jc = JAXBContext.newInstance(CubeTypeWrapper.class);
    Unmarshaller unmarshaller = jc.createUnmarshaller();
    return (CubeTypeWrapper) unmarshaller.unmarshal(
      CubeTypeTest.class.getResourceAsStream(filePath));
  }

}