package com.avrios.sample.exchange.service.storage;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.avrios.sample.exchange.service.storage.CurrencyKey.euroCurrencyKey;
import static org.assertj.core.api.Assertions.assertThat;

public class ExchangeRateServiceTest {

  private ExchangeRateService service;

  private final BigDecimal SAMPLE_RATE = new BigDecimal("12.34");

  @Before
  public void setUp() throws Exception {
    service = new InMemoryExchangeRateService();

    service.add(euroCurrencyKey("PLN"), LocalDate.now(), SAMPLE_RATE);
  }

  @Test(expected = RateNotFoundException.class)
  public void shouldThrowExceptionIfRateForGivenCurrencyPairDoesntExist() throws Exception {
    service.rateFor(euroCurrencyKey("chf"), LocalDate.now());
  }

  @Test(expected = RateNotFoundException.class)
  public void shouldThrowExceptionIfRateForGivenDayExist() throws Exception {
    service.rateFor(euroCurrencyKey("pln"), LocalDate.now().minusDays(10));
  }

  @Test
  public void shouldReturnRequestRate() throws Exception {
    assertThat(service.rateFor(euroCurrencyKey("pln"), LocalDate.now())).isEqualTo(SAMPLE_RATE);
  }

  @Test
  public void shouldReturnTrueIfRatesForGivenDayExists() throws Exception {
    assertThat(service.hasRatesFor(LocalDate.now())).isTrue();
  }
}