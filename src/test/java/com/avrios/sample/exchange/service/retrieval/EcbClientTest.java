package com.avrios.sample.exchange.service.retrieval;

import com.avrios.sample.exchange.config.ExchangeProperties;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EcbClientTest {

  private ExchangeProperties properties;
  @Mock
  private RestTemplate restTemplate;

  private EcbClient client;

  private static final String DAILY_RATES_URL = "http://localhost/daily";
  private static final String LAST_90_DAYS_URL = "http://localhost/last90";

  @Before
  public void setUp() throws Exception {
    properties = new ExchangeProperties();
    ;
    properties.setDailyRatesUrl(DAILY_RATES_URL);
    properties.setLast90DaysRatesUrl(LAST_90_DAYS_URL);

    client = new EcbClient(properties, restTemplate);
  }

  @Test(expected = EcbClientFailException.class)
  public void shouldThrowExceptionWhenEcbNotReturning200OK() throws Exception {
    when(restTemplate.getForEntity(DAILY_RATES_URL, CubeTypeWrapper.class))
      .thenReturn(ResponseEntity.badRequest().build());

    client.retrieveTodaysRates();
  }

  @Test(expected = EcbClientFailException.class)
  public void shouldThrowExceptionWhenEcbResponseHasNoBody() throws Exception {
    when(restTemplate.getForEntity(LAST_90_DAYS_URL, CubeTypeWrapper.class))
      .thenReturn(ResponseEntity.ok().build());

    client.retrieveLast90DaysRates();
  }

  @Test(expected = EcbClientFailException.class)
  public void shouldThrowExceptionWhenEcbResponseHasMoreThenOneValueForDailyRates() throws Exception {
    when(restTemplate.getForEntity(DAILY_RATES_URL, CubeTypeWrapper.class))
      .thenReturn(ResponseEntity.ok(create(new CubeType(), new CubeType())));

    client.retrieveLast90DaysRates();
  }

  @Test
  public void shouldReturnListOfCubeTypesWhenEcbDailyReturns200OKWithBody() throws Exception {
    when(restTemplate.getForEntity(DAILY_RATES_URL, CubeTypeWrapper.class))
      .thenReturn(ResponseEntity.ok(create(new CubeType())));

    assertThat(client.retrieveTodaysRates()).isNotNull();
  }

  @Test
  public void shouldReturnListOfCubeTypesWhenEcbLast90Returns200OKWithBody() throws Exception {
    when(restTemplate.getForEntity(LAST_90_DAYS_URL, CubeTypeWrapper.class))
      .thenReturn(ResponseEntity.ok(create(new CubeType())));

    assertThat(client.retrieveLast90DaysRates().size()).isEqualTo(1);
  }

  private CubeTypeWrapper create(CubeType... rates) {
    final CubeTypeWrapper wrap = new CubeTypeWrapper();
    final CubeType parent = new CubeType();
    parent.setContent(Lists.newArrayList(rates));
    wrap.setParent(parent);
    return wrap;
  }
}