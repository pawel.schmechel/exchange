package com.avrios.sample.exchange.service.storage;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class CurrencyKeyTest {

  @Test
  public void shouldBeEqualRegardlessOfCase() throws Exception {
    assertThat(new CurrencyKey("eur", "pln")).isEqualTo(new CurrencyKey("EUR", "PLN"));
  }
}