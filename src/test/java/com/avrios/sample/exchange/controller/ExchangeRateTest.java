package com.avrios.sample.exchange.controller;

import com.avrios.sample.exchange.controller.ExchangeController.ExchangeRate;
import com.avrios.sample.exchange.service.storage.ExchangeRateService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.avrios.sample.exchange.service.storage.CurrencyKey.euroCurrencyKey;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ExchangeRateTest {

  @Autowired
  private TestRestTemplate restTemplate;
  @Autowired
  private ExchangeRateService exchangeService;

  @Before
  public void setUp() throws Exception {
    exchangeService.add(euroCurrencyKey("PLN"), LocalDate.now(), BigDecimal.ONE);
  }

  @Test
  public void shouldReturnExchangeRateEURtoPLNForToday() {
    ExchangeRate rate = restTemplate.getForObject("/EUR/PLN", ExchangeRate.class);

    assertThat(rate.getRate()).isEqualTo(BigDecimal.ONE);
  }

  @Test
  public void shouldReturn404WhenRateCouldntBeFound() {
    ResponseEntity<ExchangeRate> res = restTemplate.getForEntity("/EUR/CHF", ExchangeRate.class);

    assertThat(res.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
  }

}