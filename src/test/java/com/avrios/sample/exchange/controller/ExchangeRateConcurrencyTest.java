package com.avrios.sample.exchange.controller;

import com.avrios.sample.exchange.service.storage.ExchangeRateService;
import com.avrios.sample.exchange.service.storage.RateNotFoundException;
import lombok.extern.slf4j.Slf4j;
import net.jodah.concurrentunit.ConcurrentTestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Random;

import static com.avrios.sample.exchange.service.storage.CurrencyKey.euroCurrencyKey;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class ExchangeRateConcurrencyTest extends ConcurrentTestCase {

  @Autowired
  private ExchangeRateService exchangeService;

  @Test
  public void shouldSupportConcurrentAddingAndReadingOfRates() throws Throwable {
    final LocalDate now = LocalDate.now();
    final int threadCount = 50;

    threadedRun(threadCount, () -> {
      try {
        final Random random = new Random();
        for (int i = 0; i < 1000 * 1000; i++) {
          if (random.nextBoolean()) {
            exchangeService.add(
              euroCurrencyKey("PL" + random.nextInt(10)), now.minusDays(random.nextInt(90)), new BigDecimal(i));
          } else {
            try {
              exchangeService.rateFor(
                euroCurrencyKey("PL" + random.nextInt(10)), now.minusDays(random.nextInt(90)));
            } catch (RateNotFoundException e) {
              // ignore
            }
          }
        }
      } catch (Exception e) {
        log.error("Test failed", e);
        threadFail(e);
      }

      resume();
    });

    await(100000, threadCount);
  }

  public static void threadedRun(int threadCount, Runnable runnable) {
    Thread[] threads = new Thread[threadCount];

    for (int i = 0; i < threadCount; i++) {
      threads[i] = new Thread(runnable);
    }

    for (int i = 0; i < threadCount; i++) {
      threads[i].start();
    }
  }
}
