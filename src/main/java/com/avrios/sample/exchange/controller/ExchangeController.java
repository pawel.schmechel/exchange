package com.avrios.sample.exchange.controller;

import com.avrios.sample.exchange.service.storage.ExchangeRateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.avrios.sample.exchange.service.storage.CurrencyKey.euroCurrencyKey;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController("/exchange")
@Slf4j
public class ExchangeController {

  private final ExchangeRateService exchangeRateService;

  @Autowired
  public ExchangeController(ExchangeRateService exchangeRateService) {this.exchangeRateService = exchangeRateService;}

  @RequestMapping(value = "/EUR/{currency}", method = GET, produces = APPLICATION_JSON_VALUE)
  public ExchangeRate euroToAnotherCurrencyToday(@PathVariable String currency) {
    return euroToAnotherCurrency(currency, LocalDate.now());
  }

  @RequestMapping(value = "/EUR/{currency}/{day}", method = GET, produces = APPLICATION_JSON_VALUE)
  public ExchangeRate euroToAnotherCurrency(
    @PathVariable String currency,
    @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate day) {
    log.debug("Checking exchange rate {}-{} on {}", "EUR", currency, day);
    return new ExchangeRate(exchangeRateService.rateFor(euroCurrencyKey(currency), day));
  }

  static class ExchangeRate {
    private BigDecimal rate;

    @SuppressWarnings("unused")
    ExchangeRate() {}

    ExchangeRate(BigDecimal rate) {
      this.rate = rate;
    }

    public BigDecimal getRate() { return rate; }

    public void setRate(BigDecimal rate) { this.rate = rate; }
  }
}
