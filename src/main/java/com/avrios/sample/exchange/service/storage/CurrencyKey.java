package com.avrios.sample.exchange.service.storage;

import com.google.common.base.Objects;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;


@ToString
public final class CurrencyKey {
  private final String from;
  private final String to;

  CurrencyKey(String from, String to) {
    this.from = StringUtils.upperCase(from);
    this.to = StringUtils.upperCase(to);
  }

  public static CurrencyKey euroCurrencyKey(String currencyTo) {
    return new CurrencyKey("EUR", currencyTo);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    CurrencyKey that = (CurrencyKey) o;
    return Objects.equal(from, that.from) && Objects.equal(to, that.to);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(from, to);
  }
}
