package com.avrios.sample.exchange.service.storage;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class RateNotFoundException extends RuntimeException {
  public RateNotFoundException(String msg) {
    super(msg);
  }
}
