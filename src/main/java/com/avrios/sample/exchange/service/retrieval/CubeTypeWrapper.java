package com.avrios.sample.exchange.service.retrieval;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collections;
import java.util.List;

@XmlRootElement(name = "Envelope", namespace = "http://www.gesmes.org/xml/2002-08-01")
@XmlAccessorType(XmlAccessType.FIELD)
@Setter
@Getter
@ToString
public class CubeTypeWrapper {

  @XmlElement(name = "Cube", namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref", type = CubeType.class)
  private CubeType parent;

  public List<CubeType> getRates() {
    return parent != null ? parent.getContent() : Collections.emptyList();
  }
}
