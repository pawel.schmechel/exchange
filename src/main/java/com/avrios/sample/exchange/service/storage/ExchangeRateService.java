package com.avrios.sample.exchange.service.storage;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface ExchangeRateService {
  BigDecimal rateFor(CurrencyKey key, LocalDate day);

  void add(CurrencyKey key, LocalDate day, BigDecimal exchangeRate);

  boolean hasRatesFor(LocalDate day);
}
