package com.avrios.sample.exchange.service.job;

import com.avrios.sample.exchange.service.retrieval.CubeType;
import com.avrios.sample.exchange.service.retrieval.EcbClient;
import com.avrios.sample.exchange.service.storage.ExchangeRateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

import static com.avrios.sample.exchange.service.storage.CurrencyKey.euroCurrencyKey;

@Component
@Slf4j
public class UpdateRatesJob {

  private final EcbClient ecbClient;
  private final ExchangeRateService rateService;

  @Autowired
  public UpdateRatesJob(EcbClient ecbClient, ExchangeRateService rateService) {
    this.ecbClient = ecbClient;
    this.rateService = rateService;
  }

  @Scheduled(cron = "${app.scheduling.dailyRatesCron}")
  public void updateDailyRates() {
    log.debug("Daily rates update triggered...");

    final LocalDate now = LocalDate.now();

    if (!rateService.hasRatesFor(now)) {
      ecbClient.retrieveTodaysRates().getContent()
        .forEach(ct -> updateForDay(ct, now));
    }
  }

  private void updateForDay(CubeType ct, LocalDate day) {
    log.debug("Updating value {} for {}", ct, day);
    rateService.add(euroCurrencyKey(ct.getCurrency()), day, new BigDecimal(ct.getRate()));
  }

  @Scheduled(cron = "${app.scheduling.last90DaysCron}")
  public void updateLast90DaysRates() {
    log.debug("Last 90 days rates update triggered...");

    if (!hasAlreadyRetrievedRatesForLast90Days()) {
      ecbClient.retrieveLast90DaysRates()
        .forEach(
          ct -> ct.getContent().forEach(
            dailyCt -> updateForDay(dailyCt, LocalDate.parse(ct.getTime()))));
    }
  }

  // Rates are only provided for working days,
  // If system has them stored for previous Monday - assume all are OK
  // of course this whole check can also be disabled, and the rates will be downloaded on each schedule
  private boolean hasAlreadyRetrievedRatesForLast90Days() {
    return rateService.hasRatesFor(LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.MONDAY)));
  }
}
