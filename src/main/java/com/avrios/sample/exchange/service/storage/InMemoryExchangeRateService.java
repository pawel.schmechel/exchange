package com.avrios.sample.exchange.service.storage;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class InMemoryExchangeRateService implements ExchangeRateService {
  private static Map<LocalDate, Map<CurrencyKey, BigDecimal>> rates = new ConcurrentHashMap<>();

  @Override
  public BigDecimal rateFor(CurrencyKey key, LocalDate day) {
    if (!rates.containsKey(day) || !rates.get(day).containsKey(key)) {
      throw new RateNotFoundException(
        String.format("Exchange rate %s for %s couldn't be found.", key, day));
    }

    return rates.get(day).get(key);
  }

  @Override
  public void add(CurrencyKey key, LocalDate day, BigDecimal exchangeRate) {
    rates.putIfAbsent(day, new ConcurrentHashMap<>());
    rates.get(day).put(key, exchangeRate);
  }

  @Override
  public boolean hasRatesFor(LocalDate day) {
    return rates.containsKey(day);
  }

}
