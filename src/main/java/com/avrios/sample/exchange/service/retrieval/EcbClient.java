package com.avrios.sample.exchange.service.retrieval;

import com.avrios.sample.exchange.config.ExchangeProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@Slf4j
public class EcbClient {
  private final String dailyRatesUrl;
  private final String last90DaysRatesUrl;

  private final RestTemplate restTemplate;

  @Autowired
  public EcbClient(ExchangeProperties properties, RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
    dailyRatesUrl = properties.getDailyRatesUrl();
    last90DaysRatesUrl = properties.getLast90DaysRatesUrl();
  }

  public CubeType retrieveTodaysRates() {
    List<CubeType> rates = get(dailyRatesUrl, CubeTypeWrapper.class).getRates();

    if (rates.size() != 1) {
      throw new EcbClientFailException("Bad response from daily ECB, size should be 1, is " + rates.size());
    }

    return rates.get(0);
  }

  public List<CubeType> retrieveLast90DaysRates() {
    return get(last90DaysRatesUrl, CubeTypeWrapper.class).getRates();
  }

  private <T> T get(String url, Class<T> responseType) {
    try {
      ResponseEntity<T> res = restTemplate.getForEntity(url, responseType);

      if (res.getStatusCode() != HttpStatus.OK || !res.hasBody()) {
        log.error("ECB response error: {}", res);
        throw new EcbClientFailException("Bad ECB response " + res);
      }

      return res.getBody();
    } catch (Exception ex) {
      log.error("ECB call failed.", ex);
      throw new EcbClientFailException(ex);
    }
  }

}
