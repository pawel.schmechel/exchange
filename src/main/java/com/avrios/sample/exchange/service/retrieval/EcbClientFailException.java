package com.avrios.sample.exchange.service.retrieval;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
public class EcbClientFailException extends RuntimeException {
  public EcbClientFailException(Exception ex) {
    super(ex);
  }

  public EcbClientFailException(String msg) {
    super(msg);
  }
}
