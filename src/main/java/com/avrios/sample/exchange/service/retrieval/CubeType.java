
package com.avrios.sample.exchange.service.retrieval;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CubeType", namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref", propOrder = {
  "content"
})
@Getter
@Setter
@ToString
public class CubeType {

  @XmlElement(name = "Cube", namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref", type = CubeType.class)
  private List<CubeType> content;
  @XmlAttribute(name = "currency")
  private String currency;
  @XmlAttribute(name = "rate")
  private String rate;
  @XmlAttribute(name = "time")
  private String time;

  public List<CubeType> getContent() {
    if (content == null) {
      content = new ArrayList<>();
    }
    return this.content;
  }

}
