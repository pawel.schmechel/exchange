package com.avrios.sample.exchange.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("exchange")
@Getter
@Setter
public class ExchangeProperties {
  private String dailyRatesUrl;
  private String last90DaysRatesUrl;
}
