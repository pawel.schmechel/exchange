# How to run the application

1. Install maven 3.3.9
2. In project root execute: **mvn spring-boot run**
3. Rest exchange service is available under **http://localhost:8080/exchange**
4. Swagger available under **http://localhost:8080/swagger-ui.html**